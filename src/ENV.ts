import { config } from 'dotenv';
import * as path from 'path';

export function toBool(value: string): boolean {
  return value === 'true';
}

config({
  path: path.join(process.cwd(), `${process.env.NODE_ENV || 'dev'}.env`),
});

export const ENV = {
  postGreDB: {
    type: 'postgres',
    host: process.env.TYPEORM_HOST,
    port: process.env.TYPEORM_PORT,
    username: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    database: process.env.TYPEORM_DATABASE,
    synchronize: process.env.TYPEORM_SYNCHRONIZE,
    logging: process.env.TYPEORM_LOGGING,
  },
};

export const ormConfig: any = {
  type: 'postgres',
  host: ENV.postGreDB.host,
  port: ENV.postGreDB.port,
  username: ENV.postGreDB.username,
  password: ENV.postGreDB.password,
  database: ENV.postGreDB.database,
  synchronize: toBool(ENV.postGreDB.synchronize),
  logging: toBool(ENV.postGreDB.logging),
};
