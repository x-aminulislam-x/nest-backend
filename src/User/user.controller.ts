import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UserService) {}

  @Get()
  getUsers(): any {
    return this.usersService.getUsers();
  }
  @Get(':id')
  find(@Param('id') id: number): any {
    return this.usersService.find(id);
  }

  @Post('create')
  @HttpCode(200)
  createUser(@Body() data): any {
    return this.usersService.createUser(data);
  }

  @Patch('update/:id')
  updateUser(@Param('id') id: number, @Body() data): any {
    return this.usersService.updateUser(id, data);
  }

  @Delete('delete/:id')
  deleteUser(@Param('id') id: number): any {
    return this.usersService.deleteUser(id);
  }

  @Delete('delete')
  deleteAllUser(): any {
    return this.usersService.deleteAllUser();
  }

  @Put('login')
  login(@Body() body: any): any {
    return this.usersService.login(body);
  }
}
