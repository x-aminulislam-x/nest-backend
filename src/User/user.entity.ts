import { Client } from 'src/client/client.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column({ nullable: true })
  name: string;
  @Column({ nullable: true })
  email: string;
  @Column({ nullable: true })
  password: string;

  @Column()
  age: number;

  @Column({ default: true })
  isActive: boolean;

  @OneToMany(() => Client, (model) => model.user)
  clients: Client[];
}
