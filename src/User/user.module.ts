import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientModule } from 'src/client/client.module';
import { UsersController } from './user.controller';
import { User } from './user.entity';
import { UserService } from './user.service';

@Module({
  imports: [TypeOrmModule.forFeature([User]), ClientModule],
  controllers: [UsersController],
  providers: [UserService, UsersController],
  exports: [UserService],
})
export class UserModule {}
