import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { ClientService } from 'src/client/ client.service';
import { Connection, Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private connection: Connection,
    private clientService: ClientService,
  ) {}

  getUsers(): any {
    return this.usersRepository.find({
      relations: ['clients'],
      select: {
        id: true,
        name: true,
        email: true,
        age: true,
        isActive: true,
      },
    });
  }

  async createUser(data) {
    const user = await this.usersRepository.findOne({
      where: { email: data.email },
    });

    const email = (await user)?.email;

    if (!!email) {
      return { status: 'email already exist' };
    }

    const hashPass = await bcrypt.hash(data.password, 10);
    return this.usersRepository.save({ ...data, password: hashPass });
  }

  updateUser(id, data): any {
    return this.usersRepository.update(id, data);
  }

  find(id: number): any {
    return this.usersRepository.findOne({
      where: { id: id },
    });
  }

  async findOne(email: string): Promise<any> {
    const user = await this.usersRepository.findOne({
      where: { email: email },
    });
    // console.log(user);
    return user;
  }

  async login({ email, password }: { email: any; password: string }) {
    const user = await this.usersRepository.findOne({
      where: { email: email },
    });
    const isMatch = await bcrypt.compare(password, user.password);
    if (isMatch) {
      delete user.password;
      return { ...user, loginStatus: 'successfully login' };
    } else {
      return { loginStatus: "email or password isn't match" };
    }
  }

  async deleteUser(id: number): Promise<any> {
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      const user: any = await this.usersRepository.find({
        where: { id: id },
        select: {
          id: true,
          name: false,
          isActive: false,
          email: false,
          password: false,
          age: false,
          clients: true,
        },
        relations: ['clients'],
      });

      const [{ clients }] = user;

      const newClients = clients?.map((item) => ({
        ...item,
        userId: 'd698af98-5c66-43eb-bcd8-26b1dd52316d',
      }));

      await this.clientService.saveWithTransaction(queryRunner, newClients);
      const dd = await queryRunner.manager.delete(User, {
        id: '56aed4a2-0dee-4c3d-8cff-b240d3c24c5b',
      });
      console.log(dd);
      if (dd.affected < 1) {
        await queryRunner.rollbackTransaction();
        return 'something went wrong';
      }
      // await this.usersRepository.delete('id');
      await queryRunner.commitTransaction();
      return { status: 'successfully deleted!' };
      // return await this.usersRepository.delete(id);
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw new BadRequestException(err.message);
    } finally {
      await queryRunner.release();
    }
  }

  async deleteAllUser() {
    // const allUser = await this.usersRepository.find();
    // const allUserIds = (await allUser).map((item) => item.id);
    // console.log(allUserIds);
    return this.usersRepository.delete({});
  }
}
