import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { QueryRunner, Repository } from 'typeorm';
import { Client } from './client.entity';

@Injectable()
export class ClientService {
  constructor(
    @InjectRepository(Client) private clientRepository: Repository<Client>,
  ) {}

  create(data: any): any {
    return this.clientRepository.save(data);
  }

  async saveWithTransaction(queryRunner: QueryRunner, data: any): Promise<any> {
    return await queryRunner.manager.save(Client, data);
  }

  getClient(): any {
    return this.clientRepository.find();
  }

  getSingleClient(id: any): any {
    return this.clientRepository.findOne({ where: { id: id } });
  }

  getClientUserData(id: any): any {
    return this.clientRepository.findOne({
      where: { id: id },
      select: {
        id: true,
        name: true,
        user: {
          id: true,
          name: true,
          age: true,
        },
      },
      relations: ['user'],
    });
  }

  updateClient(data: any) {
    return this.clientRepository.update(data.id, data);
  }

  deleteClient(id: any) {
    return this.clientRepository.delete(id);
  }
}
