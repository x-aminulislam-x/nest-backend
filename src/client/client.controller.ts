import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ClientService } from './ client.service';

@Controller('client')
export class ClientController {
  constructor(private readonly clientService: ClientService) {}

  @Post('create')
  createClient(@Body() data) {
    return this.clientService.create(data);
  }

  @Get('get')
  getClient() {
    return this.clientService.getClient();
  }

  @Get('get/:id')
  getSingleClient(@Param('id') id: any) {
    return this.clientService.getSingleClient(id);
  }

  @Get('userData')
  getClientUserData(@Body() { id }) {
    console.log(id);
    return this.clientService.getClientUserData(id);
  }

  @Patch('update')
  updateClient(@Body() data) {
    return this.clientService.updateClient(data);
  }

  @Delete('delete')
  deleteClient(@Body() { id }) {
    return this.clientService.deleteClient(id);
  }
}
