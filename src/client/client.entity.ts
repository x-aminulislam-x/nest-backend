import { User } from 'src/User/user.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Client {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @ManyToOne(() => User, (user) => user.clients)
  user: User;

  @Column()
  userId: string;

  @Column()
  name: string;

  @Column()
  typeOfClient: string;

  @Column()
  isActive: boolean;
}
